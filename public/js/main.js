
//----------------------------------------
angular.module("EndiApp", [])
.controller('AppController', function($scope) {
    console.info("AppController -> initialized");

    $scope.navContactClick = function(){
    	console.info("navContactClick");
    };
});
//----------------------------------------



(function($) {
  "use strict";

/*
  //facebook code embeed
  (function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=183323708482158";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));
  */

	//center object vertically
	function centerVertically(obj,parent_obj_string){
		var a = parseInt(obj.parents(""+parent_obj_string+"").outerHeight()),
			b = parseInt(obj.outerHeight()),
			c = (a-b)/2;
		obj.css("top",c);
	}	
	
	//open/close the contact section
	$("#company_contact_text").on("click",function(){
		if($(this).hasClass("isOpened")){
			$(this).removeClass("isOpened");
			$("#contact_to_be_trigered").removeClass("isOpened");
		} else{
			var offset = $("#company_contact_text").offset().top;
			$(this).addClass("isOpened");
			$("#contact_to_be_trigered").addClass("isOpened");
			$("html, body").animate({scrollTop:offset});
		}
	});		
		
	$(".open_footer_contact").on("click",function(e){
		e.preventDefault();
		if(document.documentElement.clientWidth < 850){
			$("nav > ul").fadeToggle();
		}	
		$("#menuMobileToggle").toggleClass("toggled");
		var offset = $("#company_contact_text").offset().top;
		if($("#company_contact_text").hasClass("isOpened")){
			$("html, body").animate({scrollTop:offset});
		} else{
			$("html, body").animate({scrollTop:offset},function(){
				$("#company_contact_text").addClass("isOpened");
				$("#contact_to_be_trigered").addClass("isOpened");
				$("html, body").animate({scrollTop:offset});
			});		
		}
	});
	
	//initialize mixItUp
	$(document).ready(function(){		
		$("#mixItUp").mixitup({
			targetSelector: '.mix',
			filterSelector: '.filtering li',
			gridClass: 'toGrid',
			listClass: 'toList',		
			layoutMode: 'grid'
		});
	});
	
	//toggle between list and grid view
	var tl = $(".has_hover").parents("#mixItUp").hasClass("toList");
	$(".has_hover").hover(function(){
		if(tl === false){
			$(this).find(".is_hover").stop().fadeIn(200);
		}
	},function(){
		if(tl == false){
			$(this).find(".is_hover").stop().fadeOut(200);
		}
	});	
	$("#toGrid").on("click",function(e){
		e.preventDefault();
		$('#mixItUp').mixitup('toGrid');
		$("#controller > ul li").removeClass("active");
		$(this).addClass("active");		
		tl = false;
	});
	$("#toList").on("click",function(e){
		e.preventDefault();
		$('#mixItUp').mixitup('toList');
		$("#controller > ul li").removeClass("active");
		$(this).addClass("active");
		tl = true;
	});
	
	//toggle the display of the filtering list in different situations
	$("#filter_projects p").on("click",function(e){
		e.preventDefault();
		e.stopImmediatePropagation();
		$("#filter_projects").toggleClass("clicked");
		if($("#filter_projects ul").css("display")!=="block"){
			$("#filter_projects ul").stop().fadeIn();
		} else $("#filter_projects ul").stop().fadeOut();
	})
	$("#filter_projects").on('click',function(e){
		e.stopImmediatePropagation();
	});
	$("body").on('click',function(){
		if($("#filter_projects").length > 0 && $("#filter_projects ul").css("display") =='block'){
			$("#filter_projects").toggleClass('clicked');
			$("#filter_projects ul").stop().fadeOut();
		}
	})
	$(".expand_submenu").on("click",function(e){		
		e.preventDefault();
		var index = $(this).parent().index();
		console.log(index);
		$(".l1_submenu").each(function(){
			var index2 = $(this).parent().index();
			if(index2 != index){
				if($(this).css("display") === "block"){
					$(this).slideToggle();
				}
			}
		});
		$(this).parent().find(".l1_submenu").slideToggle();
	});
	
	//toggle the display of the menu on mobile
	$("#menuMobileToggle").on("click",function(e){
		e.preventDefault();
		$('div.nav.top-nav').slideToggle(300);
		$("#menuMobileToggle").toggleClass("toggled");
	})
	
	//link pages
	if($('.link_pages').children().length > 0){
		$('.link_pages p').css('margin-bottom','40px');
	}
	
	var we_have_slider = 0;	
	if($(".rslides").length > 0) we_have_slider = 1;
	//initialize the big slider
	if(we_have_slider) {
		$(".rslides").responsiveSlides({
		  auto: true,             // Boolean: Animate automatically, true or false
		  speed: 500,            // Integer: Speed of the transition, in milliseconds
		  timeout: 6000,          // Integer: Time between slide transitions, in milliseconds
		  nav: false,
		});
		setTimeout(function(){
			centerVertically($("#big_slider .prev"),"#big_slider");
			centerVertically($("#big_slider .next"),"#big_slider");
		},300);
	}
	
	//needed for moving the left sidebar around
	var moved = 0,
		sizeW = document.documentElement.clientWidth;
	function moveLeftSidebar(sizeW){
		//move the left sidebar around if needed		
		if($('.left_sidebar').length > 0) {
			var myDomHolder = $('.left_sidebar');
			
			if(sizeW <= 850 && moved == 0){
				$('.left_sidebar').remove();
				myDomHolder.insertAfter('.main_post_container');
				moved = 1;	
			} else if(sizeW >= 850 && moved == 1) {
				$('.left_sidebar').remove();
				myDomHolder.insertBefore('.main_post_container');
				moved = 0;
			}
		}
	}
	moveLeftSidebar(sizeW);
	
	
	//actions on the resize of the window
	$(window).on('resize',function(){
		var sizeOfWindow = document.documentElement.clientWidth;
		if($("div.work").length > 0){
			if(sizeOfWindow < 1280){
				$("#wrapper_footer").css({"margin-top":0});
				$("body").css("background-color","#1a1c1f");
			} else {
				$("#wrapper_footer").css({"margin-top":"40px"});
				$("body").css("background-color","#fff");
			}
		}
		//center arrows
		if(we_have_slider){
			centerVertically($("#big_slider .prev"),"#big_slider");
			centerVertically($("#big_slider .next"),"#big_slider");
		}
		
		moveLeftSidebar(sizeOfWindow);		
	});
	
	//remove empty elements that can sometimes appear from the post formats
	$('.featured').each(function(){
		if(!$(this).children().length > 0){$(this).remove();}
	});

	//make videos responsive
	(function make_videos_responsive(){		
		$("iframe").each(function(){
			if($(this).parents('div.featured_audio').length > 0 || $(this).parents('div.format-audio').length > 0){}
			else{
				$(this).wrap('<div class="iframeWrapper"></div>');
			}
		})
	})();	

	//project navigator styling
	if($("#next_project").length > 0){
		if($("#prev_project").length > 0){}
		else{
			$("#next_project").css({'border-bottom-right-radius':'4px',	'border-top-right-radius':'4px', '-moz-border-bottom-right-radius':'4px', '-moz-border-top-right-radius':'4px'});
		}
	} else $("#prev_project").css({'border-bottom-right-radius':'4px',	'border-top-right-radius':'4px', '-moz-border-bottom-right-radius':'4px', '-moz-border-top-right-radius':'4px'});	
	
	//move stylesheets around
	(function(){
		if($('.dark-stylesheet').length > 0){
			var style = $('.dark-stylesheet');
			$('.dark-stylesheet').remove();
			$('head').append(style);
		}
		if($('style.options-output').length > 0){
			var style = $('style.options-output');
			$('style.options-output').remove();
			$('head').append(style);
		}
	})()
	
	$('#company_contact_text').css('border-top-color',$('#company_contact_text').css('background-color'));
	
})(jQuery);