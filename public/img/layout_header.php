<?php
$ROOT_FIX = $GLOBALS["ROOT_FIX"];
?>
<header class="section">
    <div class="container_m">
        <nav class=" group">
            <a href="<?=$ROOT_FIX?>" id="logo">

                <img src="<?=$ROOT_FIX?>img/logo.png" alt="Honest" />
            </a>
            <a href="#" id="menuMobileToggle"></a>
            <div class="nav top-nav clearfix">
                <ul id="menu-main" class="nav top-nav clearfix">
                    <li id="menu-item-57" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-57">
                    <a href="<?=$ROOT_FIX?>home/<?=$GLOBALS["token"]?>" class="<?php if($GLOBALS["active"]=="Home"){echo "active";} ?>" >Home</a>
                    </li>
                    <li id="menu-item-57" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-57">
                    <a href="<?=$ROOT_FIX?>team/<?=$GLOBALS["token"]?>" class="<?php if($GLOBALS["active"]=="Team"){echo "active";} ?>">Team</a>
                    </li>
                    <li id="menu-item-57" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-57">
                    <a href="<?=$ROOT_FIX?>work/<?=$GLOBALS["token"]?>" class="<?php if($GLOBALS["active"]=="Games"){echo "active";} ?>">Games</a>
                    </li>
                    <li id="menu-item-58" class="open_footer_contact menu-item menu-item-type-custom menu-item-object-custom menu-item-58">
                    <a ng-click="navContactClick();" href="#" class="<?php if($GLOBALS["active"]=="Contact"){echo "active";} ?>">Contact</a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>
