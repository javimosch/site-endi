<?php
define('ROOT'		, dirname(__FILE__) );
define('ROOTPATH'	, dirname($_SERVER["REQUEST_URI"]) . "/" );
define('PUBLIC_TOKEN', "12");
define('MASTER_PASSWORD', "123456");
require ROOT . '/libs/flight/Flight.php';  
include ROOT . "/routes/route.work.php";
$ROOT_FIX = ROOTPATH;
//-------------------
Flight::route('/', function(){
    //Flight::redirect('./home');
    Flight::redirect("./home/".base64_encode(PUBLIC_TOKEN)); //PUBLIC  KEY
    //Flight::redirect("./home/".base64_encode(MASTER_PASSWORD));  //PRIVATE KEY	 
});

Flight::route('/login', function(){
    //Flight::redirect('./home');
    //Flight::redirect("./home/".base64_encode(PUBLIC_TOKEN)); //PUBLIC  KEY
    Flight::redirect("./home/".base64_encode(MASTER_PASSWORD));  //PRIVATE KEY	 
});
Flight::route('/home/login', function(){
    Flight::redirect("./home/".base64_encode(MASTER_PASSWORD));  //PRIVATE KEY	 
});

Flight::route('/home(/@token)', function($token){
	Flight::fixROOTPATH($token);
    include ROOT . "/views/page_home.php";
});
Flight::route('/work(/@token)', function($token){
	Flight::fixROOTPATH($token);
    include ROOT . "/views/page_work.php";
});
Flight::route('/team(/@token)', function($token){
	Flight::fixROOTPATH($token);
    include ROOT . "/views/page_team.php";
});
Flight::route("GET /accessRestricted",function(){
	$GLOBALS["token"] = "0";
	//$GLOBALS["ROOT_FIX"] = "./";
	echo "<!doctype html>";
	echo "<html lang='en-US'>";
	include ROOT . "/views/layout_head.php";
	echo "<body>";
	include ROOT . "/views/layout_header.php";
	echo "<div class='wrapper'>";
	include ROOT . "/views/content_accessRestricted.php"; 
	echo "</div>";
	include ROOT . "/views/layout_contact.php";
	include ROOT . "/views/layout_footer.php"; 
	include ROOT . "/views/layout_scripts.php"; 
	echo "</body>";
	echo "</html>";
});

Flight::map('fixROOTPATH', function($token){
	if(isset($token)){
		$GLOBALS["ROOT_FIX"] = dirname(ROOTPATH) . "/";
	}else{
	   	//$GLOBALS["ROOT_FIX"] = "../";
	}
	$GLOBALS["token"] = $token;
	if(!isset($GLOBALS["token"]) || $GLOBALS["token"] == ""){
		$GLOBALS["token"] = base64_encode(PUBLIC_TOKEN); //No token ?. Take this public token boy.
	}
});
//-------------------

Flight::map('notFound', function(){
	$GLOBALS["token"] = "0";
	//$GLOBALS["ROOT_FIX"] = "./";
    include ROOT . "/views/page_notfound.php";
});

Flight::start();
?>
