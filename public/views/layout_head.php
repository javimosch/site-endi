<?php
	$ROOT_FIX = $GLOBALS["ROOT_FIX"];
?>

<head ng-app="EndiApp" ng-controller="AppController">
	<!--
	<meta charset="utf-8">
	-->
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<title>Endi Studios</title>		
	
	<!--<link rel="pingback" href="http://renascenthemes.com/demo/honest/xmlrpc.php">-->

	<link rel="shortcut icon" type="image/png" href="<?=$ROOT_FIX?>img/favicon.png"/>
	<style>                
	#header{
	margin: 0 auto;
	}            
	</style>
	
<!--
	<link rel="alternate" type="application/rss+xml" title="Honest &raquo; Feed" href="http://renascenthemes.com/demo/honest/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Honest &raquo; Comments Feed" href="http://renascenthemes.com/demo/honest/comments/feed/" />
	<link rel="alternate" type="application/rss+xml" title="Honest &raquo; Home Comments Feed" href="http://renascenthemes.com/demo/honest/home/feed/" />
	-->
	
	<link rel='stylesheet' id='contact-form-7-css'  href='<?=$ROOT_FIX?>css/endi.tmpl.css' type='text/css' media='all' />
	<link rel='stylesheet' id='responsive-lightbox-swipebox-front-css'  href='<?=$ROOT_FIX?>css/endi.swipebox.css' type='text/css' media='all' />
	<link rel='stylesheet' id='Open Sans-css'  href='http://fonts.googleapis.com/css?family=Open+Sans%3A300%2C400%2C600%2C700%2C800&#038;ver=3.9.1' type='text/css' media='all' />
	<link rel='stylesheet' id='Montserrat-css'  href='http://fonts.googleapis.com/css?family=Montserrat%3A700&#038;ver=3.9.1' type='text/css' media='all' />
	<link rel='stylesheet' id='rthemes-stylesheet-css'  href='<?=$ROOT_FIX?>css/endi.tmpl_dos.css' type='text/css' media='all' />
	<link href='http://fonts.googleapis.com/css?family=Cinzel:400,700,900' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>
	
	<link rel='stylesheet' id='rthemes-stylesheet-css'  href='<?=$ROOT_FIX?>css/endi.games.css' type='text/css' media='all' />
	<link rel='stylesheet' id='rthemes-stylesheet-css'  href='<?=$ROOT_FIX?>css/endi.team.css' type='text/css' media='all' />
	<link rel='stylesheet' id='rthemes-stylesheet-css'  href='<?=$ROOT_FIX?>css/endi.general.css' type='text/css' media='all' />

	<script type='text/javascript' src='<?=$ROOT_FIX?>libs/jquery.min.js'></script>
	<script type='text/javascript' src='<?=$ROOT_FIX?>libs/jquery-migrate.min.js'></script>

	<script type='text/javascript' src='<?=$ROOT_FIX?>libs/angular.min.js'></script>


	<script type='text/javascript' src='<?=$ROOT_FIX?>libs/jquery.swipebox.min.js'></script>
	<script type='text/javascript'>
	/* <![CDATA[ */
	var rlArgs = {"script":"swipebox","selector":"lightbox","custom_events":"","activeGalleries":"1","animation":"1","hideBars":"1","hideBarsDelay":"5000","videoMaxWidth":"1080"};
	/* ]]> */
	</script>
	<script type='text/javascript' src='<?=$ROOT_FIX?>js/front.js'></script>
	<script type='text/javascript' src='<?=$ROOT_FIX?>libs/modernizr-2.5.3-min.js'></script>

	<!--
	<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://renascenthemes.com/demo/honest/xmlrpc.php?rsd" />
	<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://renascenthemes.com/demo/honest/wp-includes/wlwmanifest.xml" /> 
	<link rel='next' title='Journal' href='http://renascenthemes.com/demo/honest/journal/' />
	<meta name="generator" content="WordPress 3.9.1" />
	<link rel='canonical' href='http://renascenthemes.com/demo/honest/' />
	<link rel='shortlink' href='http://renascenthemes.com/demo/honest/' />
	-->
</head>