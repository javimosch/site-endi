
<?php
	$GLOBALS["active"] = "Home";
?>

<!doctype html>
<html lang="en-US">
	<?php include ROOT . "/views/layout_head.php"; ?>
<body>
	<?php include ROOT . "/views/layout_header.php"; ?>
	<div class="wrapper">
		<div id="main_content" class="section">
			<?php include ROOT . "/views/content_home.php"; ?>
		</div>
	</div>
	<?php include ROOT . "/views/layout_contact.php"; ?>
	<?php include ROOT . "/views/layout_footer.php"; ?>
	<?php include ROOT . "/views/layout_scripts.php"; ?>
</body>
</html>