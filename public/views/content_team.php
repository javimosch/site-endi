
<div class="team section group" id="home_about">
    <div class="container_m">
        <div class="endi text small">
            MORE THAN A TEAM
        </div>
        <div class="endi text big">
            A BROTHERHOOD OF HEROES
        </div>
        <!--
        <div class="endi text medium light">
            (FOR A MODERATE FEE)
        </div>
-->
          <h3>
            Endi is more than software, graphics or lines of code. 
            <br>
           Its a family of people bringing creativity, guts and skills into everything we do,
            <br>
            and no matter how big or small is the challenge, we enjoy it.
            <br>
            For us quality is everything.
            <br>
            
        </h3>
       
        
        <div class="team section">
            <div id="main_content" class="team section collection group">
                <ul id="mixItUp" class="team toGridTeam">
                    <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                            
                                <img src="<?=$ROOT_FIX?>img/team/gabriel_finances_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">

                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Gabriel</span>
                                <span class="hover_description">Finances</span>
                            </a>
                        </div>
                    </li>
                      <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                                <img src="<?=$ROOT_FIX?>img/team/santiago_art_director_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Santiago</span>
                                <span class="hover_description">Art Director</span>
                            </a>
                        </div>
                    </li>
                     <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                                <img src="<?=$ROOT_FIX?>img/team/adrian_animator_director_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Adrian</span>
                                <span class="hover_description">Animation Director</span>
                            </a>
                        </div>
                    </li>
                    <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                                <img src="<?=$ROOT_FIX?>img/team/adrian_animator_director_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Lucre</span>
                                <span class="hover_description">Business</span>
                            </a>
                        </div>
                    </li>
                     <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                                <img src="<?=$ROOT_FIX?>img/team/dami_business_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Dami</span>
                                <span class="hover_description">Business Director</span>
                            </a>
                        </div>
                    </li>
                    <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                                <img src="<?=$ROOT_FIX?>img/team/ariel_art_director_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Ariel</span>
                                <span class="hover_description">Product Manager</span>
                            </a>
                        </div>
                    </li>
                     <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                                <img src="<?=$ROOT_FIX?>img/team/buba_lead_programmer_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Buba</span>
                                <span class="hover_description">Lead Developer</span>
                            </a>
                        </div>
                    </li>
                    <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                                <img src="<?=$ROOT_FIX?>img/team/santi_game_designer_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Santi</span>
                                <span class="hover_description">Game Designer</span>
                            </a>
                        </div>
                    </li>
                    <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                                <img src="<?=$ROOT_FIX?>img/team/javi_senior_programmer_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Javi</span>
                                <span class="hover_description">Senior Programmer</span>
                            </a>
                        </div>
                    </li>
                      <li id="post-121" class="team mix all group mix_all" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                        <!--
                            <a href="#">
                                <img src="<?=$ROOT_FIX?>img/team/maru_project_manager_thumbnail.jpg" class="attachment-rthemes-thumb-409 wp-post-image" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
                            </a>
                            -->
                            <a href="#" class="team is_hover">
                                <span class="hover_title">Maru</span>
                                <span class="hover_description">Project Manager</span>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
             

        

        </div>
    </div>
</div>

