
<?php
	$GLOBALS["active"] = "Games";
?>

<!doctype html>
<html lang="en-US">
	<?php include ROOT . "/views/layout_head.php"; ?>
<body>
	<?php include ROOT . "/views/layout_header.php"; ?>
	<div class="wrapper">
    	<div class="container_m group work">
			<?php include ROOT . "/views/content_work.php"; ?>
		</div>
	</div>
	<?php include ROOT . "/views/layout_contact.php"; ?>
	<?php include ROOT . "/views/layout_footer.php"; ?>
	<?php include ROOT . "/views/layout_scripts.php"; ?>
</body>
</html>