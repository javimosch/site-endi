<div id="page_title_container" class="clearfix">
    <h3>Portfolio</h3>
    <div id="project_navigator">
        <ul class="group">
            <li>
                <a id="prev_project" 
                href="<?=$ROOT_FIX?>work" title="" style="border-bottom-right-radius: 4px; border-top-right-radius: 4px;"></a>
            </li>
            <li>
                <a href="<?=$ROOT_FIX?>work" id="close_project"></a>
            </li>
        </ul>
    </div>
</div>

<div class="project_container group">
    <div class="group">
        <div id="project_description" class="span_8_of_12 col">
            <div class="blog_post">
                <img class="alignnone size-full "  src="<?=$ROOT_FIX?>resources/disney_fairies/1.jpg" alt="rise1">
                <img class="alignnone size-full "  src="<?=$ROOT_FIX?>resources/disney_fairies/2.jpg" alt="rise2">
                <img class="alignnone size-full "  src="<?=$ROOT_FIX?>resources/disney_fairies/3.jpg" alt="rise2">
             </div>
        </div>
        <div id="project_info" class="span_4_of_12 col">
            <h1>Fairies</h1>

<p class="project_info_description">Believing is just the beginning with TinkerBell and all her Fairies friends.Players create a unique fairy using a wide range of appearance options and explore the world 
    as miniature fairies while making friends, and playing an assortment of other mini games.</p>

            <p id="project_client">
                <span>Client:</span>
                <span></span>
            </p>
            <p id="project_type">
                <span>Services:</span>
                <span>Web Game</span>
            </p>
            <p id="project_date">
                <span>Date:</span>
                <span>March 2013</span>
            </p>
            <a href="#" id="visit_project">launch project</a>
            
        </div>
    </div>
    
</div>
