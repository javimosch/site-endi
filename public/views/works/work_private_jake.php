<div id="page_title_container" class="clearfix">
    <h3>Portfolio</h3>
    <div id="project_navigator">
        <ul class="group">
            <li>
                <a id="prev_project" 
                href="<?=$ROOT_FIX?>work" title="New Branding Project" style="border-bottom-right-radius: 4px; border-top-right-radius: 4px;"></a>
            </li>
            <li>
                <a href="<?=$ROOT_FIX?>work" id="close_project"></a>
            </li>
        </ul>
    </div>
</div>

<div class="project_container group">
    <div class="group">
        <div id="project_description" class="span_8_of_12 col">
            <div class="blog_post">
                <img class="alignnone size-full wp-image-20"  src="<?=$ROOT_FIX?>resources/jake/1.jpg" alt="rise1">
                <img class="alignnone size-full wp-image-19"  src="<?=$ROOT_FIX?>resources/jake/2.jpg" alt="rise2">
                <img class="alignnone size-full wp-image-122" src="<?=$ROOT_FIX?>resources/jake/3.jpg" alt="rise3">
             </div>
        </div>
        <div id="project_info" class="span_4_of_12 col">
            <h1>Jake and the Neverland Pirates</h1>

<p class="project_info_description">Jake and his faithful crew of Never Land Pirates: Cubby, Izzy, and Scully band together to enjoy awesome adventures. 
    Join the Fun and help the team outsmart Captain Hook in this fast paced full of danger and non-stop fun.</p>

            <p id="project_client">
                <span>Client:</span>
                <span>Disney</span>
            </p>
            <p id="project_type">
                <span>Services:</span>
                <span>Web Game</span>
            </p>
            <p id="project_date">
                <span>Date:</span>
                <span>April 2014</span>
            </p>
            <a href="#" id="visit_project">launch project</a>
            
        </div>
    </div>
    
</div>
