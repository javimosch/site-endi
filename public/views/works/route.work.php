<?php

//TRABAJOS PUBLICOS ///@_user_id:[0-9]+ ----------------------------------------------------------
Flight::route('GET /work/rise-of-hero(/@token)', function($token){
	Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_public_rise");
});
Flight::route('GET /work/abducer(/@token)', function($token){
	Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_public_abducer");
});
Flight::route('GET /work/axn-forensic(/@token)', function($token){
	Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_public_axnforensic");
});
Flight::route('GET /work/beloved(/@token)', function($token){
	Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_public_beloved");
});
Flight::route('GET /work/cars(/@token)', function($token){
	Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_public_cars");
});
Flight::route('GET /work/coke-fabrica-alegria(/@token)', function($token){
	Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_public_cokefabricaalegria");
});
Flight::route('GET /work/doors(/@token)', function($token){
	Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_public_doors");
});
Flight::route('GET /work/nike(/@token)', function($token){
	Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_public_nike");
});


Flight::map("fixROOTPATHANDTOKEN",function($token){
	$GLOBALS["token"] = $token;
	$GLOBALS["ROOT_FIX"] = dirname(dirname(ROOTPATH)) . "/";

	if(!isset($GLOBALS["token"]) || $GLOBALS["token"] == ""){
		$GLOBALS["token"] = base64_encode(PUBLIC_TOKEN); //No token ?. Take this public token boy.
		$GLOBALS["ROOT_FIX"] = dirname(ROOTPATH) . "/";
	}else{
		//$GLOBALS["ROOT_FIX"] = dirname(ROOTPATH) . "/";
	}
	
	$GLOBALS["active"] = "Games"; //active !
});


//TRABAJOS PRIVADOS ----------------------------------------------------------
Flight::route('GET /work/disney-faries/@token', function($token){
    //Flight::renderWorkSinglePrivate("work_private_disneyfaries",$token);
    Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_private_disneyfaries");
});
Flight::route('GET /work/flappy-chicken/@token', function($token){
    //Flight::renderWorkSinglePrivate("work_private_flappychicken",$token);
    Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_private_flappychicken");
});
Flight::route('GET /work/jake/@token', function($token){
    //Flight::renderWorkSinglePrivate("work_private_jake",$token);
    Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_private_jake");
});
Flight::route('GET /work/malefica/@token', function($token){
    //Flight::renderWorkSinglePrivate("work_private_malefica",$token);
    Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_private_malefica");
});
Flight::route('GET /work/planes1/@token', function($token){
    //Flight::renderWorkSinglePrivate("work_private_planes1",$token);
    Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_private_planes1");
});
Flight::route('GET /work/planes2/@token', function($token){
    //Flight::renderWorkSinglePrivate("work_private_planes2",$token);
    Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_private_planes2");
});
Flight::route('GET /work/superchibi/@token', function($token){
    //Flight::renderWorkSinglePrivate("work_private_superchibi",$token);
    Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_private_superchibi");
});
Flight::route('GET /work/tmnt/@token', function($token){
    //Flight::renderWorkSinglePrivate("work_private_tmnt",$token);
    Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_private_tmnt");
});
Flight::route('GET /work/esfera/@token', function($token){
    //Flight::renderWorkSinglePrivate("work_private_esfera",$token);
    Flight::fixROOTPATHANDTOKEN($token);
    Flight::renderWorkSinglePublic("work_private_esfera");
});



//FUNCIONES AUXILIARES ----------------------------------------------------------


Flight::map("renderWorkSinglePublic",function($name){
	echo "<!doctype html>";
	echo "<html lang='en-US'>";
	include ROOT . "/views/layout_head.php";
	echo "<body>";
	include ROOT . "/views/layout_header.php";
	echo "<div class='wrapper'>";
	echo "<div class='container_m group project'>";
	include ROOT . "/views/works/" . $name . ".php"; 
	echo "</div>";
	echo "</div>";
	include ROOT . "/views/layout_contact.php";
	include ROOT . "/views/layout_footer.php"; 
	include ROOT . "/views/layout_scripts.php"; 
	echo "</body>";
	echo "</html>";
});

Flight::map("renderWorkSinglePrivate",function($name,$token){
	$passDecoded = base64_decode($token);
	if($passDecoded == MASTER_PASSWORD){
		$GLOBALS["ROOT_FIX"] = dirname(dirname(ROOTPATH)) . "/";
		Flight::renderWorkSinglePublic($name);
	}else{
		Flight::redirect('./accessRestricted');
	}
});





?>