<div id="page_title_container" class="clearfix">
    <h3>Portfolio</h3>
    <div id="project_navigator">
        <ul class="group">
            <li>
                <a id="prev_project" 
                href="<?=$ROOT_FIX?>work" title="New Branding Project" style="border-bottom-right-radius: 4px; border-top-right-radius: 4px;"></a>
            </li>
            <li>
                <a href="<?=$ROOT_FIX?>work" id="close_project"></a>
            </li>
        </ul>
    </div>
</div>

<div class="project_container group">
    <div class="group">
        <div id="project_description" class="span_8_of_12 col">
            <div class="blog_post">
                <img class="alignnone size-full wp-image-20"  src="<?=$ROOT_FIX?>resources/esfera/1.jpg" alt="rise1">
                <img class="alignnone size-full wp-image-19"  src="<?=$ROOT_FIX?>resources/esfera/2.jpg" alt="rise2">
                <img class="alignnone size-full wp-image-122" src="<?=$ROOT_FIX?>resources/esfera/3.jpg" alt="rise3">
             </div>
        </div>
        <div id="project_info" class="span_4_of_12 col">
            <h1>Esfera Universal</h1>

<p class="project_info_description">A war raging from the beginning of time is about end after a cataclysm of unknown origins. An elite clan of vampires, an army of angels and the remains of the human race are in for quest of survival. But the balance of power between them turns even bloodier when a 
    beautiful young angel warrior fall in love with the beautiful and ruthless vampire princess Larumar and together they have to decide the fate of the world.</p>

            <p id="project_client">
                <span>Client:</span>
                <span>Beth Van Garret</span>
            </p>
            <p id="project_type">
                <span>Services:</span>
                <span>Interactive book for ipad</span>
            </p>
            <p id="project_date">
                <span>Date:</span>
                <span>July 2014</span>
            </p>
            <a href="#" id="visit_project">launch project</a>
            
        </div>
    </div>
    
</div>
