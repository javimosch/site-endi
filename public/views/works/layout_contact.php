<!-- contact sections -->
<div class="section group" id="company_contact_text">
    <div class="container_m">
        <p>
            <span>CONTACT US</span>
        </p>
        <p>Get your game on!</p>
    </div>
</div>

<div class="section group " id="contact_to_be_trigered">
    <div class="container_m contains_page">

        <p style="text-align: center;">Thou Will, our command</p>
        <ul class="group container_m" id="contact_info">
            <li id="contact_contact">
                <h5>contact</h5>
                <p>
                    <a href="mailto:office@websitename.com">office@websitename.com</a>
                </p>
                <p class="phone">
                    +40 712 345 678                    
                </p>
            </li>
            <li id="contact_location">
                <h5>location</h5>
                <p>Endi Street 1234</br>World of Endi/li>
            <li id="contact_social">
                <h5>Elsewhere</h5>
            </li>
        </ul>
        
        <!--

        <h2 style="text-align: center;"></h2>
        <h2 style="text-align: center;">Make an enquiry</h2>
        <p style="text-align: center;">
            <div class="wpcf7" id="wpcf7-f124-o1">
                <div class="screen-reader-response"></div>
                <form action="/demo/honest/#wpcf7-f124-o1" method="post" class="wpcf7-form" novalidate="novalidate">
                    <div style="display: none;">
                        <input type="hidden" name="_wpcf7" value="124" />
                        <input type="hidden" name="_wpcf7_version" value="3.8" />
                        <input type="hidden" name="_wpcf7_locale" value="en_US" />
                        <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f124-o1" />
                        <input type="hidden" name="_wpnonce" value="74e5a7eab2" />
                    </div>
                    <p>Your Name (required)
                        <br />
                        <span class="wpcf7-form-control-wrap your-name">
                            <input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required" aria-required="true" aria-invalid="false" />
                        </span>
                    </p>
                    <p>Your Email (required)
                        <br />
                        <span class="wpcf7-form-control-wrap your-email">
                            <input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email" aria-required="true" aria-invalid="false" />
                        </span>
                    </p>
                    <p>Subject
                        <br />
                        <span class="wpcf7-form-control-wrap your-subject">
                            <input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text" aria-invalid="false" />
                        </span>
                    </p>
                    <p>Your Message
                        <br />
                        <span class="wpcf7-form-control-wrap your-message">
                            <textarea name="your-message" cols="40" rows="10" class="wpcf7-form-control wpcf7-textarea" aria-invalid="false"></textarea>
                        </span>
                    </p>
                    <p>
                        <input type="submit" value="Send Message" class="wpcf7-form-control wpcf7-submit" />
                    </p>
                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                </form>
            </div>
        </p>
        -->
    </div>
</div>
