<div id="page_title_container" class="clearfix">
    <h3>Portfolio</h3>
    <div id="project_navigator">
        <ul class="group">
            <li>
                <a id="prev_project" 
                href="<?=$ROOT_FIX?>work" title="New Branding Project" style="border-bottom-right-radius: 4px; border-top-right-radius: 4px;"></a>
            </li>
            <li>
                <a href="<?=$ROOT_FIX?>work" id="close_project"></a>
            </li>
        </ul>
    </div>
</div>

<div class="project_container group">
    <div class="group">
        <div id="project_description" class="span_8_of_12 col">
            <div class="blog_post">
                <img class="alignnone size-full wp-image-20"  src="<?=$ROOT_FIX?>resources/axn_forensic/1.jpg" alt="axn_forensic">
                <img class="alignnone size-full wp-image-19"  src="<?=$ROOT_FIX?>resources/axn_forensic/2.jpg" alt="axn_forensic">
                <img class="alignnone size-full wp-image-122" src="<?=$ROOT_FIX?>resources/axn_forensic/3.jpg" alt="axn_forensic">
             </div>
        </div>
        <div id="project_info" class="span_4_of_12 col">
            <h1>axn forensic</h1>

<p class="project_info_description">Are you ready to try your crime solving skills?
    <br>
    here's your chance to discover the truth behind the hit tv-series "csi"

    with this interactive hidden object game. 
<br>
    Multiple levels, hundreds of cases and unlimetd amount of fun.
    Plus, integrated tie-in cross platform advertising.
    <br>
    There's no mystery about the success of this game.</p>

            <p id="project_client">
                <span>Client:</span>
                <span>axn networks</span>
            </p>
            <p id="project_type">
                <span>Services:</span>
                <span>Ipad Game</span>
            </p>
            <p id="project_date">
                <span>Date:</span>
                <span>April 2014</span>
            </p>
            <a href="#" id="visit_project">launch project</a>
            
        </div>
    </div>
    
</div>
