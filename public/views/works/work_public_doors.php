<div id="page_title_container" class="clearfix">
    <h3>Portfolio</h3>
    <div id="project_navigator">
        <ul class="group">
            <li>
                <a id="prev_project" 
                href="<?=$ROOT_FIX?>work" title="" style="border-bottom-right-radius: 4px; border-top-right-radius: 4px;"></a>
            </li>
            <li>
                <a href="<?=$ROOT_FIX?>work" id="close_project"></a>
            </li>
        </ul>
    </div>
</div>

<div class="project_container group">
    <div class="group">
        <div id="project_description" class="span_8_of_12 col">
            <div class="blog_post">
                <img class="alignnone size-full "  src="<?=$ROOT_FIX?>resources/doors/1.jpg" alt="rise1">
                <img class="alignnone size-full "  src="<?=$ROOT_FIX?>resources/doors/2.jpg" alt="rise2">
                <img class="alignnone size-full "  src="<?=$ROOT_FIX?>resources/doors/3.jpg" alt="rise2">
                <img class="alignnone size-full "  src="<?=$ROOT_FIX?>resources/doors/4.jpg" alt="rise2">
                <img class="alignnone size-full "  src="<?=$ROOT_FIX?>resources/doors/5.jpg" alt="rise2">
             </div>
        </div>
        <div id="project_info" class="span_4_of_12 col">
            <h1>Doors of the Mind Inner Mysteries</h1>

<p class="project_info_description">Jump into Hidden Object action and explore a world of nightmares in this mysterious adventure, full of puzzles, mini-games and spooky gameplay.
    Unlock the secrets of your mind and discover the dark secrets hidden within.</p>

            <p id="project_client">
                <span>Client:</span>
                <span>Big Fish</span>
            </p>
            <p id="project_type">
                <span>Services:</span>
                <span>Desktop Game, Ipad Game, Iphone Game</span>
            </p>
            <p id="project_date">
                <span>Date:</span>
                <span>October 2012</span>
            </p>
            <a href="#" id="visit_project">launch project</a>
            
        </div>
    </div>
    
</div>
