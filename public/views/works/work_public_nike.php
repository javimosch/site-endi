<div id="page_title_container" class="clearfix">
    <h3>Portfolio</h3>
    <div id="project_navigator">
        <ul class="group">
            <li>
                <a id="prev_project" 
                href="<?=$ROOT_FIX?>work" title="New Branding Project" style="border-bottom-right-radius: 4px; border-top-right-radius: 4px;"></a>
            </li>
            <li>
                <a href="<?=$ROOT_FIX?>work" id="close_project"></a>
            </li>
        </ul>
    </div>
</div>
<h1>Nike</h1>
<div class="project_container group">
    <div class="group">
        <div id="project_description" class="span_8_of_12 col">
            <p>Praesent in convallis velit. Nulla quis mattis turpis. Curabitur porta metus sed libero commodo fermentum. Nullam porttitor libero libero, quis congue neque vehicula eu. Etiam in tempor neque, nec adipiscing dolor. Maecenas ut odio volutpat, dignissim ipsum non, egestas orci. Curabitur laoreet ante sed sodales elementum. Curabitur eu imperdiet nisl. Suspendisse ipsum urna, dapibus ac orci et, placerat varius quam. In magna tortor, ultricies ut sapien in, tincidunt faucibus tortor. Aliquam tempus auctor mauris, ac tincidunt massa eleifend in.</p>
        </div>
        <div id="project_info" class="span_4_of_12 col">
            <p id="project_client">
                <span>Client:</span>
                <span>Personal</span>
            </p>
            <p id="project_type">
                <span>Services:</span>
                <span>Mac, Wii</span>
            </p>
            <p id="project_date">
                <span>Date:</span>
                <span>April 2014</span>
            </p>
            <a href="#" id="visit_project">launch project</a>
        </div>
    </div>
    <div class="blog_post">
        <img class="alignnone size-full wp-image-20"  src="<?=$ROOT_FIX?>resources/rise/rise1.jpg" alt="rise1">
        <img class="alignnone size-full wp-image-19"  src="<?=$ROOT_FIX?>resources/rise/rise2.jpg" alt="rise2">
        <img class="alignnone size-full wp-image-122" src="<?=$ROOT_FIX?>resources/rise/rise3.jpg" alt="rise3">
    </div>
</div>
