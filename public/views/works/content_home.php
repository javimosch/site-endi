<!-- slider section -->
<div class="section group" id="big_slider">
    <ul class="rslides">
        <li>
            <img src='<?=$ROOT_FIX?>img/home_slide_1.jpg' alt='slide1' />
            <div class='slider_information_container'>
                <a class='slide_url' href='<?=$ROOT_FIX?>work'></a>
                <div class='slider_information'>
                    <div>
                        <div>
                          <!--
                            <h1 class='slide_title'>Endi Studios</h1>
                            <p>Games made by people who love games</p>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <img src='<?=$ROOT_FIX?>img/home_slide_2.jpg' alt='slide2' />
            <div class='slider_information_container'>
                <a class='slide_url' href='<?=$ROOT_FIX?>work'></a>
                <div class='slider_information'>
                    <div>
                        <div>
                           <!--
                            <h1 class='slide_title'>Endi Studios</h1>
                            <p> Games Made By Gamers</p>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </li>
        <li>
            <img src='<?=$ROOT_FIX?>img/home_slide_3.jpg' alt='slide3' />
            <div class='slider_information_container'>
                <div class='slider_information'>
                    <div>
                       <div>
                            <!--
                            <h1 class='slide_title'>Welcome to the World of Endi</h1>
                            <p>We create new worlds, fantastical characters and compelling stories for casual audiences, games that are easy to pick-up and play. Expanding beyond digital and collaborating with leading entertainment companies to create a fun gaming experience.</p>
                            -->
                        </div>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</div>


<div class="section group" id="home_about">
    
    <div class="container_m">
        
        <div class="endi text small">
           IMAGINATION IS THE POWER OF CREATIVITY
        </div>
        
        <div class="endi text big">
            WE CREATE GAMES
        </div>

        <h3>
			Endi Studios is a multi-diverse family of artists and developers
            <br> 
            Founded by industry leaders in illustration, animation and gaming entertainment
			<br>
			Driven by high quality standards and infused with a passionate heart,
            <br>
            Our goal is to create cool, fun and engaging games for all kinds of people 
            <br>
            People who work, play and dream. People like you
            <br>
            
		</h3>
       
       <!--
	<div class="endi text about one second">
	        A Quest For Fun 
	</div>
	<h3>
	    We deliver successful gaming experiences for social and casual audiences on the web, desktop and mobile devices. Games that are innovative, easy to pick up and fun to play with high quality standards and pioneering technology. No matter what idea is in your mind, we are here to make it real.
	</h3>
	-->
	      
	</div>
</div>
