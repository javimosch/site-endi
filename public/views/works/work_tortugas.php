<div id="page_title_container" class="clearfix">
    <h3>Portfolio</h3>
    <div id="project_navigator">
        <ul class="group">
            <li>
                <a id="prev_project" href="http://renascenthemes.com/demo/honest/work/new-branding-project/" title="New Branding Project" style="border-bottom-right-radius: 4px; border-top-right-radius: 4px;"></a>
            </li>
            <li>
                <a href="http://renascenthemes.com/demo/honest/work/" id="close_project"></a>
            </li>
        </ul>
    </div>
</div>
<h1>Tortugas</h1>
<div class="project_container group">
    <div class="group">
        <div id="project_description" class="span_8_of_12 col">
            <p>Go fearless into battle in this action packed awesome game. Jump, crawl, run and fight enemies in and endless maze of sewers. Chose your favorite Ninja Turtle: Raphael using Sais, Michelangelo and his lethal Nunchakus, Leonardo and the Dual Katanas or Donatello and his Bo Staff. Play different levels and fight each of the dangerous enemies. Collect Shurikens and other weapons while jumping from wall to wall and explore the dark horizons before pizza time.
The Teenage Mutant Ninja Turtles are back. And this time is personal…and mobile.
</p>
        </div>
        <div id="project_info" class="span_4_of_12 col">
            <p id="project_client">
                <span>Client:</span>
                <span>Personal</span>
            </p>
            <p id="project_type">
                <span>Services:</span>
                <span>Design, Video</span>
            </p>
            <p id="project_date">
                <span>Date:</span>
                <span>April 2014</span>
            </p>
            <a href="#" id="visit_project">launch project</a>
        </div>
    </div>
    <div class="blog_post">
        <img class="alignnone size-full wp-image-20" src="http://renascenthemes.com/demo/honest/wp-content/uploads/2014/04/tumblr_n3tswomqPS1st5lhmo1_1280.jpg" alt="tumblr_n3tswomqPS1st5lhmo1_1280">
        <img class="alignnone size-full wp-image-19" src="http://renascenthemes.com/demo/honest/wp-content/uploads/2014/04/tumblr_n3tstry6Cn1st5lhmo1_1280.jpg" alt="tumblr_n3tstry6Cn1st5lhmo1_1280">
        <img class="alignnone size-full wp-image-122" src="http://renascenthemes.com/demo/honest/wp-content/uploads/2014/04/tumblr_mopqkkwK2M1st5lhmo1_1280.jpg" alt="tumblr_mopqkkwK2M1st5lhmo1_1280">
    </div>
</div>
