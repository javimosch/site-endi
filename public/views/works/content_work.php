
<div class=" section group" id="home_about">
    <div class="container_m">
        <div class="endi text small">
            WE CREATE 
        </div>
        <div class="endi text big">
            Worlds of possibilities
        </div>
          <h3>
            New worlds with fantastical characters and compelling stories. 
            <br>
            Empowered by the coolest technologies like Starling, html5 and Unity, 
            <br>
            Games with great performance, rich visuals, clever level design and easy to pick-up and play
            <br>
            all to create a sucessful entertainment experience.  
            <br>
            This is Endi.
        </h3>
       
    </div>

</div>

<div id="page_title_container" class="clearfix">
    <h3>Portfolio</h3>
    <div id="controller" class="group">
        <div id="filter_projects">
            <p>filter gallery</p>
            <ul class="filtering">
                <li data-filter="all" class="active">All</li>
                <li data-filter="Playstation">Playstation</li>
                <li data-filter="Wii">Wii</li>
                <li data-filter="Windows">Windows</li>
                <li data-filter="Xbox360">Xbox360</li>
                <li data-filter="Mac">Mac</li>
                <li data-filter="Linux">Linux</li>
            </ul>
        </div>
        <ul class="group">
            <li id="toGrid" class="active"></li>
            <li id="toList"></li>
        </ul>
    </div>
</div>


<div class="section">
    <!-- main blog post -->
    <div id="main_content" class="section collection group">

        <ul id="mixItUp" class="toGrid">
            
            <li id="post-121" class="mix all group mix_all Playstation Wii" style="display: inline-block; opacity: 1;">
                <div class="col has_hover">
                    <a href="<?=$ROOT_FIX?>work/rise-of-hero/<?=$GLOBALS["token"]?>">
                        <img src="<?=$ROOT_FIX?>resources/rise/3.jpg" class="wp-post-image" alt="">
                    </a>
                    <a href="<?=$ROOT_FIX?>work/rise-of-hero/<?=$GLOBALS["token"]?>" class="is_hover">
                     <span class="hover_title">Rise of Hero</span>
                     <span class="hover_description">Ipad Game</span>
                    </a>
                </div>
            </li>
           
             <li id="post-117" class="mix all group mix_all Windows" style="display: inline-block; opacity: 1;">
                <div class="col has_hover">
                    <a href="<?=$ROOT_FIX?>work/cars/<?=$GLOBALS["token"]?>">
                        <img src="<?=$ROOT_FIX?>resources/cars/1.jpg" class="wp-post-image" alt="1">
                    </a>
                    <a href="<?=$ROOT_FIX?>work/cars/<?=$GLOBALS["token"]?>" class="is_hover" style="display: none;">
                        <span class="hover_title">Cars</span>
                        <span class="hover_description">Web Game</span>
                    </a>
                </div>
            </li>

   <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                <div class="col has_hover">
                    <a href="<?=$ROOT_FIX?>work/disney-faries/<?=$GLOBALS["token"]?>">
                        <img src="<?=$ROOT_FIX?>resources/disney_fairies/2.jpg" class="wp-post-image" alt="">
                    </a>
                    <a href="<?=$ROOT_FIX?>work/disney-faries/<?=$GLOBALS["token"]?>" class="is_hover">
                        <span class="hover_title">Disney Fairies</span>
                        <span class="hover_description">Web Game</span>
                    </a>
                </div>
            </li>


            <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                            <a href="<?=$ROOT_FIX?>work/tmnt/<?=$GLOBALS["token"]?>">
                                <img src="<?=$ROOT_FIX?>resources/tmnt/3.jpg" class="wp-post-image" alt="">
                            </a>
                            <a href="<?=$ROOT_FIX?>work/tmnt/<?=$GLOBALS["token"]?>" class="is_hover">
                                <span class="hover_title">TMNT</span>
                                <span class="hover_description">Web Game,ipad game</span>
                            </a>
                        </div>
            </li>

              <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                            <a href="<?=$ROOT_FIX?>work/flappy-chicken/<?=$GLOBALS["token"]?>">
                                <img src="<?=$ROOT_FIX?>resources/flappy_chicken/1.jpg" class=" wp-post-image" alt="">
                            </a>
                            <a href="<?=$ROOT_FIX?>work/flappy-chicken/<?=$GLOBALS["token"]?>" class="is_hover">
                                <span class="hover_title">Flappy Chicken</span>
                                <span class="hover_description">Mobile Game</span>
                            </a>
                        </div>
                </li>
          
               
            <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                <div class="col has_hover">
                    <a href="<?=$ROOT_FIX?>work/doors/<?=$GLOBALS["token"]?>">
                        <img src="<?=$ROOT_FIX?>resources/doors/3.jpg" class="wp-post-image" alt="">
                    </a>
                    <a href="<?=$ROOT_FIX?>work/doors/<?=$GLOBALS["token"]?>" class="is_hover">
                        <span class="hover_title">Doors of the mind</span>
                        <span class="hover_description">Desktop Game</span>
                    </a>
                </div>
            </li>

            <li id="post-45" class="mix all group mix_all Windows Xbox360 Wii " style="display: inline-block; opacity: 1;">
                <div class="col has_hover">
                    <a href="<?=$ROOT_FIX?>work/axn-forensi/<?=$GLOBALS["token"]?>">
                        <img src="<?=$ROOT_FIX?>resources/axn_forensic/1.jpg" class="wp-post-image" alt="">
                    </a>
                    <a href="<?=$ROOT_FIX?>work/axn-forensic/<?=$GLOBALS["token"]?>" class="is_hover">
                        <span class="hover_title">Axn Forensic</span>
                        <span class="hover_description">Ipad Game</span>
                    </a>
                </div>
            </li>


              <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                    <div class="col has_hover">
                        <a href="<?=$ROOT_FIX?>work/jake/<?=$GLOBALS["token"]?>">
                            <img src="<?=$ROOT_FIX?>resources/jake/1.jpg" class="wp-post-image" alt="">
                        </a>
                        <a href="<?=$ROOT_FIX?>work/jake/<?=$GLOBALS["token"]?>" class="is_hover">
                            <span class="hover_title">Jake and the pirates</span>
                            <span class="hover_description">Web Game</span>
                        </a>
                    </div>
                </li>


                     <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                            <a href="<?=$ROOT_FIX?>work/malefica/<?=$GLOBALS["token"]?>">
                                <img src="<?=$ROOT_FIX?>resources/maleficent/3.jpg" class="wp-post-image" alt="">
                            </a>
                            <a href="<?=$ROOT_FIX?>work/malefica/<?=$GLOBALS["token"]?>" class="is_hover">
                                <span class="hover_title">Maleficent</span>
                                <span class="hover_description">Web Game</span>
                            </a>
                        </div>
                    </li>

                     <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                            <a href="<?=$ROOT_FIX?>work/planes1/<?=$GLOBALS["token"]?>">
                                <img src="<?=$ROOT_FIX?>resources/planes1/1.jpg" class="wp-post-image" alt="">
                            </a>
                            <a href="<?=$ROOT_FIX?>work/planes1/<?=$GLOBALS["token"]?>" class="is_hover">
                                <span class="hover_title">Planes 1</span>
                                <span class="hover_description">Web Game</span>
                            </a>
                        </div>
                    </li>


                      <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                            <a href="<?=$ROOT_FIX?>work/planes2/<?=$GLOBALS["token"]?>">
                                <img src="<?=$ROOT_FIX?>resources/planes2/1.jpg" class="wp-post-image" alt="">
                            </a>
                            <a href="<?=$ROOT_FIX?>work/planes2/<?=$GLOBALS["token"]?>" class="is_hover">
                                <span class="hover_title">Planes 2</span>
                                <span class="hover_description">Web Game</span>
                            </a>
                        </div>
                    </li>


                      <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                        <div class="col has_hover">
                            <a href="<?=$ROOT_FIX?>work/superchibi/<?=$GLOBALS["token"]?>">
                                <img src="<?=$ROOT_FIX?>resources/super_chibi/3.jpg" class="wp-post-image" alt="">
                            </a>
                            <a href="<?=$ROOT_FIX?>work/superchibi/<?=$GLOBALS["token"]?>" class="is_hover">
                                <span class="hover_title">Super Chibi</span>
                                <span class="hover_description">Web Game</span>
                            </a>
                        </div>
                    </li>

                        <li id="post-92" class="mix all group mix_all Mac Wii" style="display: inline-block; opacity: 1;">
                            <div class="col has_hover">
                            <a href="<?=$ROOT_FIX?>work/coke-fabrica-alegria/<?=$GLOBALS["token"]?>">
                            <img src="<?=$ROOT_FIX?>resources/coke/1.jpg" class="wp-post-image" alt="">
                             </a>
                            <a href="<?=$ROOT_FIX?>work/coke-fabrica-alegria/<?=$GLOBALS["token"]?>" class="is_hover">
                            <span class="hover_title">Coke Fabrica de Alegria</span>
                            <span class="hover_description">Ipad Game</span>
                        </a>
                        </div>
                    </li>
                    <li id="post-45" class="mix all group mix_all Windows Xbox360 Wii " style="display: inline-block; opacity: 1;">
                <div class="col has_hover">
                    <a href="<?=$ROOT_FIX?>work/esfera/<?=$GLOBALS["token"]?>">
                        <img src="<?=$ROOT_FIX?>resources/esfera/esfera1.jpg" class="wp-post-image" alt="">
                    </a>
                    <a href="<?=$ROOT_FIX?>work/esfera/<?=$GLOBALS["token"]?>" class="is_hover">
                        <span class="hover_title">Esfera</span>
                        <span class="hover_description">Ipad Interactive Book</span>
                    </a>
                </div>
            </li>



           
            <?php
                if(base64_decode($GLOBALS["token"])==MASTER_PASSWORD){
                    //----------
                    ?>
                    
                    <?php
                    //----------
                }else{
                    ?>
                    

                    <?php
                }
            ?>
        </ul>

    </div>
</div>
