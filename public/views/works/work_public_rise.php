<div id="page_title_container" class="clearfix">
    <h3>Portfolio</h3>
    <div id="project_navigator">
        <ul class="group">
            <li>
                <a id="prev_project" 
                href="<?=$ROOT_FIX?>work" title="New Branding Project" style="border-bottom-right-radius: 4px; border-top-right-radius: 4px;"></a>
            </li>
            <li>
                <a href="<?=$ROOT_FIX?>work" id="close_project"></a>
            </li>
        </ul>
    </div>
</div>

<div class="project_container group">
    <div class="group">
        <div id="project_description" class="span_8_of_12 col">
            <div class="blog_post">
                <img class="alignnone size-full wp-image-20"  src="<?=$ROOT_FIX?>resources/rise/1.jpg" alt="rise1">
                <img class="alignnone size-full wp-image-19"  src="<?=$ROOT_FIX?>resources/rise/2.jpg" alt="rise2">
                <img class="alignnone size-full wp-image-122" src="<?=$ROOT_FIX?>resources/rise/3.jpg" alt="rise3">
                <img class="alignnone size-full wp-image-122" src="<?=$ROOT_FIX?>resources/rise/4.jpg" alt="rise3">
                <img class="alignnone size-full wp-image-122" src="<?=$ROOT_FIX?>resources/rise/5.jpg" alt="rise3">
             </div>
        </div>
        <div id="project_info" class="span_4_of_12 col">
            <h1>Rise of Hero</h1>

<p class="project_info_description">Travel back to the Age of the gods and join forces with the most unexpected heroes of all time into the treacherous underworld to find out your true measure and get back your fathers honor and escaping the ancient Titans that unleashed their powers
    upon the world.</p>

            <p id="project_client">
                <span>Client:</span>
                <span>Endi Studios</span>
            </p>
            <p id="project_type">
                <span>Services:</span>
                <span>Ipad Game</span>
            </p>
            <p id="project_date">
                <span>Date:</span>
                <span>April 2014</span>
            </p>
            <a href="#" id="visit_project">launch project</a>
            
        </div>
    </div>
    
</div>
